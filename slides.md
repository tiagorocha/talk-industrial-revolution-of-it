# A Revolução Industrial da TI

----  ----

## Whoami

- **Tiago Rocha**
- Engenheiro de Operações
- Especialista em Rede de Computadores
- Membro do Grupo [DevOpsPBS](https://gitlab.com/devopspbs)

----  ----

## Agenda

- Parte I: O que você deve estar careca de saber
- Parte II: O que você deveria estar por dentro
- Parte III: Ferramentas
- Parte IV: Dicas

----  ----

## Porquê Revolução Industrial

----  ----

## Parte I: O que você deve estar careca de saber

----

### Desenvolvimento Ágil de Software

É uma expressão que define um conjunto de metodologias utilizadas no desenvolvimento de software. As metodologias que fazem parte do conceito de desenvolvimento ágil, tal como qualquer metodologia de software, providencia uma estrutura conceitual para conduzir projetos de engenharia de software.

----

#### Scrum

<a href="https://www.scrum.org/"><img height="400" data-src="images/Scrum_process.svg" alt="Scrum"></a>

----

#### Kanban

<a href="https://en.wikipedia.org/wiki/Kanban_board"><img height="480" data-src="images/kanban1.png" alt="Kanban Board"></a>

----

### Lean (Sistema Toyota)

----

#### Lean Startup

<a href="https://en.wikipedia.org/wiki/The_Lean_Startup"><img height="300" data-src="images/Lean_Startup.png" alt="The Lean Startup"></a>

* The Lean Startup, Livro do [Eric Ries](https://pt.wikipedia.org/wiki/Eric_Ries)
* Metodologia para desenvolvimento de negócios e produtos

----

#### Lean Software Development

- **Principles:**
  1. Eliminate waste
  1. Amplify learning
  1. Decide as late as possible
  1. Deliver as fast as possible
  1. Empower the team
  1. Build integrity in
  1. Optimize the whole

----

### Virtualização

É o ato de criar uma versão virtual (em vez de real) de algo, incluindo a simulação de uma plataforma de hardware, sistema operacional, dispositivo de armazenamento ou recursos de rede.

----

<!-- .slide: data-background-image="images/future-of-cloud-computing.jpeg" data-background-opacity="0.2" -->

### Computação em Nuvem

----

<!-- .slide: data-background-image="images/Cloud_computing.svg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## Parte II: O que você deveria estar por dentro

----

### DevOps

<a href="https://pt.wikipedia.org/wiki/DevOps"><img height="400" data-src="images/devops-loop-illustrations.png" alt="DevOps Loop"></a>

----

<!-- .slide: data-background-image="images/image846.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

<!-- .slide: data-background-image="images/Devops-Timeline.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

###  CALMS

- Culture
- Automation
- Lean
- Measurement
- Sharing

----

### ICE

- Inclusivity
- Complex systems
- Empathy

----

### As Três Maneiras

<a href="https://itrevolution.com/the-three-ways-principles-underpinning-devops/"><img height="400" data-src="images/devops-3-maneiras.png" alt="As Três Maneiras"></a>

----

### Insfraestrutura Como Código

A infraestrutura como código é uma abordagem ao uso de tecnologias da era da nuvem para criar e gerenciar a infraestrutura dinâmica. Trata da infraestrutura e das ferramentas e serviços que gerenciam a infraestrutura em si, como um sistema de software, adaptando as práticas de engenharia de software para gerenciar mudanças no sistema de forma estruturada e segura.

----

### Gerência de Configuração

É o registro e atualização detalhada de informações que descrevem o hardware e software de uma empresa. É um processo para estabelecer e manter a consistência do desempenho de um produto, atributos funcionais e físicos com os seus requisitos, design e informação operacional ao longo de sua vida.

----

### Desenvolvimento Orientado a Testes

É uma técnica de desenvolvimento de software que se relaciona com o conceito de verificação e validação e se baseia em um ciclo curto de repetições: Primeiramente o desenvolvedor escreve um caso de teste automatizado que define uma melhoria desejada ou uma nova funcionalidade.

----

<!-- .slide: data-background-image="images/test-piramides.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Arquitetura de Microsserviços

É uma arquitetura e uma abordagem para escrever software. As aplicações são desmembradas em componentes mínimos e independentes. [Os microsserviços](https://en.wikipedia.org/wiki/Microservices) são componentes separados que trabalham juntos para realizar as mesmas tarefas. Trata-se de um componente indispensável para a otimização do desenvolvimento de aplicações para um modelo nativo em cloud.

----

### CI/CD

- Continuous Integration
- Continuous Delivery
- Continuous Deployment

----  ----

## Parte III: Ferramentas

----

<!-- .slide: data-background-image="images/periodic-table.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Shell (Linha de Comando)

<a href="https://www.gnu.org/software/bash/"><img height="200" data-src="images/gnu-bash-logo.svg" alt="Bash"></a>
&emsp;
<a href="https://microsoft.com/powershell"><img height="200" data-src="images/PowerShell_Core_6.0_icon.png" alt="PowerShell"></a>

----

### Editor/IDE

<a href="https://www.gnu.org/software/emacs/"><img height="100" data-src="images/EmacsIcon.svg" alt="Emacs"></a>
&emsp;
<a href="https://atom.io/"><img height="100" data-src="images/Atom_icon.svg" alt="Atom"></a>

<a href="https://www.vim.org/"><img height="100" data-src="images/Vimlogo.svg" alt="Vim"></a>
&emsp;
<a href="https://www.eclipse.org/"><img height="100" data-src="images/Eclipse-Luna-Logo.svg" alt="Eclipse"></a>
&emsp;
<a href="https://netbeans.apache.org/"><img height="100" data-src="images/Apache_NetBeans_Logo.svg" alt="NetBeans"></a>

<a href="https://code.visualstudio.com"><img height="100" data-src="images/Visual_Studio_Code_1.35_icon.svg" alt="VS Code"></a>
&emsp;
<a href="https://visualstudio.microsoft.com/"><img height="100" data-src="images/Visual_Studio_2017_logo_and_wordmark.svg" alt="Visual Studio"></a>

----

### Controle de Versão

<a href="https://git-scm.com/"><img height="100" data-src="images/Git-logo.svg" alt="Git"></a>
&emsp;
<a href="https://about.gitlab.com/"><img height="100" data-src="images/GitLab_logo.svg" alt="GitLab"></a>
&emsp;
<a href="https://www.assembla.com/"><img height="100" data-src="images/Assembla_Logo.png" alt="Assembla"></a>

<a href="https://github.com/"><img height="100" data-src="images/GitHub_logo_2013_padded.svg" alt="GitHub"></a>
&emsp;
<a href="https://bitbucket.org/"><img height="100" data-src="images/BitBucket_SVG_Logo.svg" alt="BitBucket"></a>

----

### ChatOps

<a href="https://slack.com/"><img height="90" data-src="images/Slack_Technologies_Logo.svg" alt="Slack"></a>
&emsp;
<a href="https://www.webex.com/"><img height="90" data-src="images/cisco-webex.png" alt="Cisco WebEx"></a>

<a href="https://rocket.chat/"><img height="90" data-src="images/logo--dark.svg" alt="Rocket.Chat"></a>
&emsp;
<a href="https://en.wikipedia.org/wiki/Internet_bot"><img height="90" data-src="images/internet-bots.png" alt="Internet Bots"></a>

<a href="https://about.mattermost.com/chatops/"><img height="90" data-src="images/Mattermost-Logo-Blue.svg" alt="Mattermost"></a>

----

### Gerência de Configuração

<a href="https://www.ansible.com/"><img height="120" data-src="images/Ansible_logo.svg" alt="Ansible"></a>
&emsp;
<a href="https://puppet.com/"><img height="120" data-src="images/Puppet_Logo.svg" alt="Puppet"></a>
&emsp;
<a href="https://www.chef.io/"><img height="120" data-src="images/Chef_logo.svg" alt="Chef"></a>
&emsp;
<a href="https://docs.microsoft.com/en-us/powershell/dsc/overview/overview"><img height="120" data-src="images/C7lH4-lVMAEPRoy.png" alt="Desired State Configuration"></a>

<a href="https://cfengine.com"><img height="100" data-src="images/CFEngine-logo.svg" alt="CFEngine"></a>
&emsp;
<a href="https://www.saltstack.com/"><img height="100" data-src="images/SaltStack-logo.png" alt="SaltStack"></a>

----

### Insfraestrutura Como Código

<a href="https://www.terraform.io/"><img height="150" data-src="images/Terraform_Logo.svg" alt="Terraform"></a>
&emsp;
<a href="https://aws.amazon.com/cloudformation/"><img height="150" data-src="images/photo.png" alt="CloudFormation"></a>

----

### Computação em Nuvem

<a href="https://aws.amazon.com/"><img height="100" data-src="images/Amazon_Web_Services_Logo.svg" alt="AWS"></a>
&emsp;
<a href="https://azure.microsoft.com/"><img height="100" data-src="images/Microsoft_Azure_Logo.svg" alt="Azure"></a>
&emsp;
<a href="https://www.openstack.org"><img height="100" data-src="images/OpenStack_Logo_2016.svg" alt="OpenStack"></a>

<a href="https://www.heroku.com/"><img height="100" data-src="images/heroku-logotype-vertical-purple.svg" alt="Heroku"></a>
&emsp;
<a href="https://cloud.google.com/"><img height="100" data-src="images/Google_Cloud_Logo.svg" alt="GCP"></a>
&emsp;
<a href="https://www.digitalocean.com/"><img height="100" data-src="images/DigitalOcean_logo.svg" alt="Digital Ocean"></a>

----

### Container

<a href="https://kubernetes.io/"><img height="120" data-src="images/eco-logo-1f4bbe50d92cdefe6eefb6fd887603e1.svg" alt="Kubernetes"></a>
&emsp;
<a href="https://www.docker.com/"><img height="120" data-src="images/vertical.png" alt="Docker"></a>
&emsp;
<a href="https://www.openshift.com/"><img height="120" data-src="images/OpenShift-LogoType.svg" alt="OpenShift"></a>

<a href="https://rancher.com/"><img height="120" data-src="images/logo-square.png" alt="Rancher"></a>
&emsp;
<a href="https://cri-o.io/"><img height="120" data-src="images/crio-logo.svg" alt="cri-o"></a>

----

### Teste

<a href="https://www.katalon.com/"><img height="90" data-src="images/katalon-studio-logo.png" alt="Katalon"></a>
&emsp;
<a href="https://www.seleniumhq.org/"><img height="90" data-src="images/Seleniumlogo.png" alt="Selenium"></a>
&emsp;
<a href="https://phpunit.de/"><img height="90" data-src="images/phpunit-300x267.png" alt="PHPUnit"></a>

<a href="https://testinfra.readthedocs.io/"><img height="90" data-src="images/testinfra-logo.svg" alt="test infra"></a>
&emsp;
<a href="https://serverspec.org/"><img height="90" data-src="images/serverspec-logo.png" alt="ServerSpec"></a>

----

### Telemetria

<a href="https://www.zabbix.com"><img height="100" data-src="images/Zabbix_logo.png" alt="Zabbix"></a>
&emsp;
<a href="https://www.nagios.org"><img height="100" data-src="images/nagios.png" alt="Nagios"></a>

<a href="https://www.elastic.co/"><img height="100" data-src="images/logo-elastic-stack-lt.svg" alt="Elastic Stack"></a>
&emsp;
<a href="https://prometheus.io/"><img height="100" data-src="images/Prometheus_software_logo.svg" alt="Prometheus"></a>
&emsp;
<a href="https://grafana.com/"><img height="100" data-src="images/grafana.svg" alt="Grafana"></a>

----

### CI/CD

<a href="https://jenkins.io/"><img height="100" data-src="images/Jenkins_logo_with_title.svg" alt="Jenkins"></a>
&emsp;
<a href="https://about.gitlab.com/product/continuous-integration/"><img height="100" data-src="images/gitlab-ci-cd-logo_2x.png" alt="Gitlab CI"></a>
&emsp;
<a href="https://travis-ci.org/"><img height="100" data-src="images/Travis_CI_Logo.svg" alt="Travis CI"></a>

----  ----

## Parte IV: Dicas

----

### Participe de Comunidades

**Grupos no Telegram**  
<a href="https://listatelegram.github.io/"><img height="100" data-src="images/Telegram_logo.svg" alt="Telegram"></a>

**Grupo DevOpsPBS**  
<a href="https://www.meetup.com/devopspbs/"><img height="100" data-src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

----

### Awesome List 

- [Awesome FullStack](https://github.com/kevindeasis/awesome-fullstack)
- [Awesome SysAdmin](https://github.com/kahun/awesome-sysadmin)
- [Awesome DevOps](https://github.com/AcalephStorage/awesome-devops)
- [Awesome Python](https://github.com/vinta/awesome-python)
- [Awesome PHP](https://github.com/ziadoz/awesome-php)
- [Awesome Ansible](https://github.com/jdauphant/awesome-ansible)
- [Awesome Docker](https://github.com/veggiemonk/awesome-docker)

----

### Os Doze Fatores

A aplicação doze-fatores é uma metodologia para construir softwares-como-serviço.

https://12factor.net/pt_br/

----

### Livros

<a href="http://www.altabooks.com.br/manual-de-devops-como-obter-agilidade-confiabilidade-e-seguranca-em-organizacoes-tecnologicas.html"><img height="200" data-src="images/devops-handbook.png" alt="DevOps Handbook"></a>
&emsp;
<a href="http://www.altabooks.com.br/o-projeto-fenix-um-romance-sobre-ti-devops-e-sobre-ajudar-o-seu-negocio-a-vencer.html"><img height="200" data-src="images/TPP_Front-Cover.jpg" alt="O Projeto Fenix"></a>
&emsp;
<a href="https://www.grupoa.com.br/entrega-continua-ebook-p988526"><img height="200" data-src="images/entrega-continua.jpg" alt="Entrega Contínua"></a>
&emsp;
<a href="https://infrastructure-as-code.com/book/"><img height="200" data-src="images/infrastructure-as-code-kief-morris.png" alt="Infrastructure as Code"></a>
&emsp;
<a href="https://novatec.com.br/livros/reliability-engineering/"><img height="200" data-src="images/sre-book.png" alt="Site Reliability Engineering"></a>
&emsp;
<a href="https://landing.google.com/sre/books/"><img height="200" data-src="images/sre-workbook.png" alt="The Site Reliability Workbook"></a>


----  ----

## Conclusão

----  ----

## Obrigado!

----  ----

## Contatos

- **Blog:** [tiagorocha.eti.br](https://tiagorocha.eti.br/)
- **E-mail:** tiagorocha at disroot.org
- **Telegram:** [@Tiag0Rocha](https://t.me/Tiag0Rocha)
- **Riot/Matrix:** tiagorocha
- **IRC:** tiagorocha (Freenode e OFTC)
